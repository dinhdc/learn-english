# About the book


- **The Curious Incident of The Dog in the Night Time** is the story of a young boy, Christopher, who has autism. Christopher goes to a special school and finds it very hard to understand and communicate with people from the outside world. He discovers that his neighbour’s dog has been murdered and decides to investigate the crime. This takes him on a terrifying and challenging journey that changes his life.

- The story is told by Christopher, which means the language is simple and easy to understand. Its short length also makes this book easy to read, even for beginners!

## Target

- Read 15 pages each day
- Note new words to file with format page_{from_page}_to_{to_page}.md
