# New Words

| Word                           | Description                                                      |
| ------------------------------ | ---------------------------------------------------------------- |
| chase (~ **work out**)         | to run, drive, etc. after somebody/something in order to catch them or it |
| knife and fork                 | a small object with three or four points and a handle, that you use to pick up food |
| fork garden                    | a tool with a long handle and three or four points, used for digging and breaking soil into pieces |
| wound                          | an injury to part of the body, especially one in which a hole is made in the skin using a weapon |
| cancer                         | a serious disease in which growths of cells, also called cancers, form in the body and kill normal body cells |
| lawn                           | an area of ground covered in short grass in a garden or park, or used for playing a game on |
| grass                          | a common wild plant with narrow green leaves and stems that are eaten by cows, horses, sheep, etc. |
| kneel                          | to be in or move into a position where your body is supported on your knee or knees |
| muzzle                         | the nose and mouth of an animal, especially a dog or a horse |
| snount                         | the long nose and area around the mouth of some types of animal, such as a pig |
| poodle                         | a dog with thick curly hair that is sometimes cut into special shapes |
| stroke                         | to move your hand gently and slowly over an animal’s fur or hair |
| pretend                        | to behave in a particular way, in order to make other people believe something that is not true |
| tear + something (adv)         | to damage something by pulling it apart or into pieces or by cutting it on something sharp; to become damaged in this way |
| lift + smt / sbd.              | to raise somebody/something or be raised to a higher position or level |
| faithfull.                     | staying with or supporting a particular person, organization or belief  ~~ **loyal** |
| scream (verb)                  | to give a loud, high shout, because you are hurt, frightened, excited, etc.  ~~ **shriek** |
| patio                          | a flat hard area outside, and usually behind, a house where people can sit |
| shouting (noun).               | shouts from a number of people |
| housecoat (noun)               | a long loose dress worn at home over underwear or night clothes |
| go out of sbd / smt.           | to be no longer present in somebody/something; to disappear from somebody/something |
| puzzle.                        | a game, etc. that you have to think about carefully in order to answer it or do it |
| murder (verb) (~ **homicide**) | the crime of killing somebody deliberately |
| mystery.                       | something that is difficult to understand or to explain |
| glide (adj).                   | to move smoothly and quietly, especially as though it takes no effort |
| leaf (noun)                    | a flat green part of a plant, growing from a stem or branch or from the root |
| squat (verb) + (down)          | to sit on your heels with your knees bent up close to your body
| upset (adj)                    | unhappy or disappointed because of something unpleasant that has happened |
| groan (verb)                   | to make a long deep sound because you are annoyed, upset or in pain, or with pleasure |

